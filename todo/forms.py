from django import forms

class TaskForm(forms.Form):
    text = forms.CharField(max_length=50, widget = forms.TextInput(attrs={'class' : 'input is-info', 'placeholder' : 'What task would you like to add?'}))
